package net.veloxity.kotlin.sample.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import net.veloxity.sdk.*
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        try {
            val vlxOptions = VeloxityOptions.Builder(applicationContext)
                .setPriority(<PRIORITY>)
                .setLicenseKey("<LICENSE_KEY>")
                .setWebServiceEndpoint("<WEB_SERVICE_URL>")
                .setFCMProjectID("<FCM_PROJECT_ID>")
                .setFCMSenderID("<FCM_SENDER_ID>")
                .setFCMAppID("<FCM_APP_ID>")
                .setFCMApiKey("<FCM_API_KEY>")

                .setDialogTitle(resources.getString(R.string.data_usage_title))
                .setDialogMessage(resources.getString(R.string.data_usage_message))
                .setListener(object : DataUsageListener {
                    override fun onDataUsageResult(isServiceStarted: Boolean) {
                        Log.i(
                            "Veloxity",
                            "Data Usage Page Accepted and Service Started: $isServiceStarted"
                        )
                    }

                    override fun onCompleted() {
                        Log.i(
                            "Veloxity",
                            "Data Usage Page is finished"
                        )
                    }
                })
                .build()
            Veloxity.initialize(vlxOptions)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
