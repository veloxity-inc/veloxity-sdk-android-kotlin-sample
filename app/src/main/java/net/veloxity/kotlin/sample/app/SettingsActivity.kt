package net.veloxity.kotlin.sample.app

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import net.veloxity.sdk.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings, SettingsFragment())
            .commit()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        private lateinit var sdkPreference: SwitchPreference

        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)

            // Service status can be read from sharedPreferences which is saved before in MainActivity
            // or can be gotten from the getServiceStatus() method of Veloxity SDK.
            val serviceStatus = Veloxity.getServiceStatus(context) == ServiceStatus.RUNNING

            sdkPreference =
                preferenceManager.findPreference<SwitchPreference>("veloxity_sdk") as SwitchPreference
            sdkPreference.isChecked = serviceStatus

            sdkPreference.setOnPreferenceChangeListener { _, any ->
                val switchPref = any as Boolean

                if (switchPref) {
                    optIn(context)
                } else {
                    optOut(context)
                }
                true
            }
        }

        private fun optOut(context: Context?) {
            Veloxity.optOut(context)
        }

        private fun optIn(context: Context?) {
            try {
                val veloxityOptions = VeloxityOptions.Builder(context)
                    .setPriority(<PRIORITY>)
                    .setLicenseKey("<LICENSE_KEY>")
                    .setWebServiceEndpoint("<WEB_SERVICE_URL>")
                    .setFCMProjectID("<FCM_PROJECT_ID>")
                    .setFCMSenderID("<FCM_SENDER_ID>")
                    .setFCMAppID("<FCM_APP_ID>")
                    .setFCMApiKey("<FCM_API_KEY>")
                    .setDialogTitle(resources.getString(R.string.data_usage_title))
                    .setDialogMessage(resources.getString(R.string.data_usage_message))
                    .setListener(object : DataUsageListener {
                        override fun onDataUsageResult(isServiceStarted: Boolean) {
                            Log.i(
                                "Veloxity",
                                "Data Usage Page Accepted and Service Started: $isServiceStarted"
                            )
                            sdkPreference.isChecked = isServiceStarted
                        }

                        override fun onCompleted() {
                            Log.i(
                                "Veloxity",
                                "Data Usage Page is finished"
                            )
                        }
                    })
                    .build()
                Veloxity.optIn(veloxityOptions)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}